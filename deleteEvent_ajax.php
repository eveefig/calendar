<?php
    header("Content-Type: application/json");
	ini_set("session.cookie_httponly", 1);
    session_start();
    require 'database.php';
    
    $token = $_POST['token'];
	if($_SESSION['token'] !== $token){
		echo json_encode(array(
    		"success" => false,
    		"message" => "Forgery Detected"
            ));
        exit;
	}
    
    $id = $_POST['id'];
    
    $stmt = $mysqli->prepare("delete from event where id=?");
        if(!$stmt){
			$error = $mysqli->error;
            echo json_encode(array(
                "success" => false,
                "message" => "Query Prep Failed: $error"
                ));
            exit;
        }
        
    $stmt->bind_param('s', $id);
	$stmt->execute();
	$stmt->close();
    
    echo json_encode(array(
       "success" => true,
       "message" => "event successfully updated"
    ));
    exit;
        
?>