<?php
    header("Content-Type: application/json");
	
	ini_set("session.cookie_httponly", 1);
	
    session_start();
    require 'database.php';
    
    $username = $_SESSION['username'];
    $token = $_POST['token'];
	if($_SESSION['token'] !== $token){
		echo json_encode(array(
    		"success" => false,
    		"message" => "Forgery Detected"
            ));
        exit;
	}
    
    $stmt = $mysqli->prepare("select id, date, start, end, title, category, sharedBy from event where owner=?");
        if(!$stmt){
			$error = $mysqli->error;
            echo json_encode(array(
                "success" => false,
                "message" => "Query Prep Failed: $error"
                ));
            exit;
        }
    $stmt->bind_param('s', $username);
	$stmt->execute();
    $stmt->bind_result($id, $date, $start, $end, $title, $category, $sharedBy);
	
	$events = array();
	$numEvents = 0;
	
    while ($stmt->fetch()) {
		++$numEvents;
		//add a dimension to events and push this 'row' of event data into it
		$events[] = array('id' => htmlentities($id), 'date' => htmlentities($date), 'start' => htmlentities($start),
						  'end' => htmlentities($end), 'title' => htmlentities($title), 'category' => htmlentities($category),
						  'sharedBy' => htmlentities($sharedBy));
	}
	
	$stmt->close();
	
	echo json_encode(array(
        "success" => true,
        "events" => $events,
		"numEvents" => $numEvents
    ));
    exit;
    
?>      