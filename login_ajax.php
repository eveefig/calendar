<?php
// login_ajax.php
 
header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
setcookie('calendar', '', time()+3600, "/~tjhuber/Module5G/calendar.html","http://ec2-52-25-247-216.us-west-2.compute.amazonaws.com", 0, 1);

$username = $_POST['username'];
$password = $_POST['password'];
 
// Check to see if the username and password are valid.  (You learned how to do this in Module 3.)
 
	
if( preg_match('/^[\w_\-]+$/', $username) && preg_match('/^[\w_\-]+$/', $password) ){
	session_start();
    require 'database.php';
      
    $stmt = $mysqli->prepare("SELECT COUNT(*), crypt_pwd FROM user WHERE username=?");
	if(!$stmt){
		echo json_encode(array(
    		"success" => false,
    		"message" => sprintf("Query Prep Failed: %s\n", $mysqli->error)
            ));
        exit;
	}

	// Bind the parameter
	$stmt->bind_param('s', $username);
	$stmt->execute();

	// Bind the results
	$stmt->bind_result($cnt, $pwd_hash);
	$stmt->fetch();

	// Compare the submitted password to the actual password hash
	if( $cnt == 1 && crypt($password, $pwd_hash)==$pwd_hash) {
		// Login succeeded!
		$_SESSION['username'] = $username;
		$_SESSION['token'] = substr(md5(rand()), 0, 10); // generate a 10-character random string
		// Redirect to your target page
        
        echo json_encode(array(
    		"success" => true,
			"token" => $_SESSION['token'],
			"user" => $username
            ));
        exit;
	}
	else{
		echo json_encode(array(
    		"success" => false,
    		"message" => "Password or Username Incorrect"
            ));
        exit;
	}
    
}else{
	echo json_encode(array(
		"success" => false,
		"message" => "Incorrectly formatted username or password."
	));
	exit;
}
?>