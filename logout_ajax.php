<?php
    header("Content-Type: application/json");
	ini_set("session.cookie_httponly", 1);
    session_start();
    
    $token = $_POST['token'];
	if($_SESSION['token'] !== $token){
		echo json_encode(array(
    		"success" => false,
    		"message" => "Forgery Detected"
            ));
        exit;
	}
    session_destroy();
    echo json_encode(array(
    	"success" => true
        ));
    exit;

?>