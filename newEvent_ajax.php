 <?php
 header("Content-Type: application/json");
	ini_set("session.cookie_httponly", 1);
    session_start();
	
    require 'database.php';
    
    $token = $_POST['token'];
	if($_SESSION['token'] !== $token){
		echo json_encode(array(
    		"success" => false,
    		"message" => "Forgery Detected"
            ));
        exit;
	}
    
    $title = $_POST['title'];
    $date = $_POST['date'];
    $start = $_POST['start'];
    $end = $_POST['end'];
	$category = $_POST['category'];
	
	
	//check that inputs are all valid
	//check that inputs are all valid
	if( !preg_match('/^[\w\s\']+$/', $title) || !preg_match('/^[0-9][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]$/', $date) ||
	   !preg_match('/^[0-2][0-9]:[0-5][0-9](:[0-5][0-9])?$/', $start) ) {
		    echo json_encode(array(
					"success" => false,
					"message" => "invalid input"
				));
	}
    else if(preg_match('/undefined/', $end)) {
		$end = "00:00:00";
	}
	else if(!preg_match('/^[0-2][0-9]:[0-5][0-9](:[0-5][0-9])?$/', $end)) {
				echo json_encode(array(
					"success" => false,
					"message" => "invalid input"
				));
				exit;
	}
    
    $stmt = $mysqli->prepare("insert into event (title, date, start, end, owner, category) values (?, ?, ?, ?, ?, ?)");
        if(!$stmt){
			$error = $mysqli->error;
            echo json_encode(array(
                "success" => false,
                "message" => "Query Prep Failed: $error"
                ));
            exit;
        }
        
    $stmt->bind_param('ssssss', $title, $date, $start, $end, $_SESSION['username'], $category);
	$stmt->execute();
	$stmt->close();
    
    echo json_encode(array(
       "success" => true,
       "message" => "event successfully added"
    ));
    exit;
        
?>