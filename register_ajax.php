<?php
    header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json
	ini_set("session.cookie_httponly", 1);
	
	$new_user = $_POST['username'];
	$new_password = $_POST['password'];
 
    // Check to see if the username and password are valid.  (You learned how to do this in Module 3.)
	
    if( preg_match('/^[\w_\-]+$/', $new_user) && preg_match('/^[\w_\-]+$/', $new_password) ){
		
		session_start();
		require 'database.php';
		
		$stmt = $mysqli->prepare("select username from user");
        if(!$stmt){
			$error = $mysqli->error;
            echo json_encode(array(
                "success" => false,
                "message" => "Query Prep Failed: $error"
                ));
            exit;
        }
        
		$stmt->execute();
		$stmt->bind_result($user);
    
		$existingUser = false;
		while($stmt->fetch()) {
		    if($user == $new_user) {
		        $existingUser = true;
		    }
		}  
		$stmt->close();
		
		if($existingUser) {
		    echo json_encode(array(
		        "success" => false,
		        "message" => "That username is already taken."
		    ));
		    exit;
		}
        
        $stmt = $mysqli->prepare("INSERT INTO user (username, crypt_pwd) values (?,?)");
        if(!$stmt){
			$error = $mysqli->error;
            echo json_encode(array(
                "success" => false,
                "message" => "Query Prep Failed: $error"
                ));
            exit;
        }
    
        $encrypted = crypt($new_password);
    
        //bind the parameter
        $stmt->bind_param('ss', $new_user, $encrypted);
        
        //insert and close
        $stmt->execute();
        $stmt->close();
    
        // Register Succeeded. Log them in and redirect to the main page.
        $_SESSION['username'] = $new_user;
        $_SESSION['token'] = substr(md5(rand()), 0, 10); // generate a 10-character random string 
    
        echo json_encode(array(
        	"success" => true,
			"token" => $_SESSION['token'],
			"user" => $new_user
        ));
        exit;
    }else{
        echo json_encode(array(
    		"success" => false,
    		"message" => "Incorrectly formatted username or password"
            ));
            exit;
    }
?>