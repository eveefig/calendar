<?php
 header("Content-Type: application/json");
	ini_set("session.cookie_httponly", 1);
    session_start();
	
    require 'database.php';
    
    $token = $_POST['token'];
	if($_SESSION['token'] !== $token){
		echo json_encode(array(
    		"success" => false,
    		"message" => "Forgery Detected"
            ));
        exit;
	}
    
    $friend = $_POST['friend'];
    $username = $_SESSION['username'];
	
	if( !preg_match('/^[\w_\-]+$/', $friend)) {
        echo json_encode(array(
    		"success" => false,
    		"message" => "Invalid Friend Username."
            ));
        exit;
    }
    
    $stmt = $mysqli->prepare("select username from user");
        if(!$stmt){
			$error = $mysqli->error;
            echo json_encode(array(
                "success" => false,
                "message" => "Query Prep Failed: $error"
                ));
            exit;
        }
        
	$stmt->execute();
    $stmt->bind_result($user);
    
    $validUser = false;
    while($stmt->fetch()) {
        if($user = $friend) {
            $validUser = true;
        }
    }  
	$stmt->close();
    
    if(!$validUser) {
        echo json_encode(array(
                "success" => false,
                "message" => "Requested friend is not a user."
                ));
            exit;
    }
    
     $stmt = $mysqli->prepare("delete from friends where (user=? and friend=?)");
        if(!$stmt){
			$error = $mysqli->error;
            echo json_encode(array(
                "success" => false,
                "message" => "Query Prep Failed: $error"
                ));
            exit;
        }
    
    $stmt->bind_param('ss', $username, $friend);
	$stmt->execute();
	$stmt->close();
    
    echo json_encode(array(
       "success" => true,
       "message" => "Friend successfully removed."
    ));
    exit;
        
?>