<?php
header("Content-Type: application/json");
	ini_set("session.cookie_httponly", 1);
    session_start();
	
    require 'database.php';
    
	$token = $_POST['token'];
	$trueToken = $_SESSION['token'];
	if($trueToken !== $token){
		echo json_encode(array(
    		"success" => false,
    		"message" => "Forgery Detected"
            ));
        exit;
	}
    
    //$friend = $_POST['friend'];
    $username = $_SESSION['username'];
	$friend = array();
	$fr =mysqli_query("select user from friends where friend = '$username'");
	while($f = mysqli_fetch_assoc($fr)){
		echo "SomeLine";
	
	
//	if( !preg_match('/^[\w_\-]+$/', $friend) ) {
//        echo json_encode(array(
//    		"success" => false,
//    		"message" => "Invalid Friend Username."
//            ));
//        exit;
//    }
    
    $stmt = $mysqli->prepare("select user from friends where friend=?");
        if(!$stmt){
			$error = $mysqli->error;
            echo json_encode(array(
                "success" => false,
                "message" => "Query Prep Failed: $error"
                ));
            exit;
        }
    $stmt->bind_param('s', $username);    
	$stmt->execute();
    $stmt->bind_result($check_user);
    
    $validUser = false;
    while($stmt->fetch()) {
        if($friend == $check_user) {
            $validUser = true;
        }
    }  
	$stmt->close();
    
    if(!$validUser) {
        echo json_encode(array(
                "success" => false,
                "message" => "That user has not friended you."
        ));
        exit;
    }
    
    $title = $_POST['title'];
    $date = $_POST['date'];
    $start = $_POST['start'];
    $end = $_POST['end'];
	$category = $_POST['category'];
	
	
	//check that inputs are all valid
	if( !preg_match('/^[\w\s\']+$/', $title) || !preg_match('/^[0-9][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]$/', $date) ||
	   !preg_match('/^[0-2][0-9]:[0-5][0-9](:[0-5][0-9])?$/', $start) ) {
		    echo json_encode(array(
					"success" => false,
					"message" => "invalid input"
			));
			exit;
	}
    else if(preg_match('/undefined/', $end)) {
		$end = "00:00:00";
	}
	else if(!preg_match('/^[0-2][0-9]:[0-5][0-9](:[0-5][0-9])?$/', $end)) {
		echo json_encode(array(
			"success" => false,
			"message" => "invalid input"
		));
		exit;
	}
    
    $stmt = $mysqli->prepare("insert into event (title, date, start, end, owner, category, sharedBy) values (?, ?, ?, ?, ?, ?, ?)");
        if(!$stmt){
			$error = $mysqli->error;
            echo json_encode(array(
                "success" => false,
                "message" => "Query Prep Failed: $error"
                ));
            exit;
        }
        
    $stmt->bind_param('sssssss', $title, $date, $start, $end, $friend, $category, $username);
	$stmt->execute();
	$stmt->close();
    
    echo json_encode(array(
       "success" => true,
       "message" => "event successfully shared"
    ));
    exit;
	}    
?>